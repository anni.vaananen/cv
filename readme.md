### [Anni Vaananen](https://www.linkedin.com/in/annivaananen) | Developer | London

Driven and hardworking developer with a diverse skill set. Experience in web development, database management, scripting and release management. I’m seeking a full-stack role where I can leverage my skills, contribute to exciting projects, and keep learning as part of a great team.

### Work Experience

#### Engineer | Yellowbrick Data | 11/2018 - 11/2024

**Automation and release processes:**
- Automated and scripted the release validation process, and developed a new Python releaser tool. Implemented JIRA integration and commit validation, to ensure Jira and repos were in sync. This automation eliminated time-consuming previously manual tasks and ensured a structured release process.
- Oversaw 20+ successful releases between 2022/2023, ensuring that all relevant validations were executed and verified.

**Performance Engineering:**
- Developing and maintaining the Python performance runner, a tool used to run, measure and analyze performance.
- Maintaining, running, and analyzing performance suites; validating new hardware; isolating and reporting regressions; flagging and investigating plan changes; and memory analysis.


**Quality Engineering:**
- Developed and maintained test suites using SQL, bash, and Python. Conducted quality engineering activities, including test planning, execution, and reporting.
- Automation, test development, and maintenance with SQL, bash, and Python. Validated and tested new features, improvements, and bug fixes.

---

#### Web developer | Pancentric Digital | 10/2015 - 11/2018
**Full stack development:**
- Developed front-end and back-end components for multi-site CMSs using Wagtail CMS.
- Created micro sites using Node.js, enhancing performance and user experience.
- Built APIs using Django Rest API and Node to facilitate efficient data exchange.
- Contributed to support and maintenance tasks, troubleshooting issues and enhancing performance across various sites and stacks.
- Played an active role in constructing new features for existing sites, optimizing functionality.
- Executed front-end builds for Sitecore & Django CMS sites, ensuring responsive and engaging user interfaces.
- Built internal applications, including a React Native iPad app for sign-ins and React apps for helpdesk ticket monitoring and calendar data visualization.

---

#### Accounting & Bookkeeping | Konekesko Ltd | 04/2012 - 05/2015
- Comprehensive bookkeeping, invoicing, and regular reporting, while collaborating closely with sales and managerial teams. Leveraged an in-depth understanding of financial components to impact the company's overall profitability.
- In a role akin to financial controlling, I actively collaborated with my supervisor to enhance operational processes. This joint effort resulted in streamlining workflows, ultimately reducing the team's size and increasing overall efficiency.

---

#### Skills
- Programming Languages: Python, SQL, Bash, JavaScript
- Web Development: HTML, CSS, React, Node.js, WagtailCMS
- API Development: Python, Node.js
- Database Management: Performance analysis, regression testing
- Automation: Scripting, test frameworks
- Collaboration Tools: JIRA, BitBucket, Git
- Process Improvement: Release validation, efficiency enhancement


#### Education & certificates
- Estonian Business School | B.A Sc International Business Administration | 2009-2013
- [Founders & Coders](http://www.foundersandcoders.com/) | 8 week full-time coding bootcamp | 05/2015 - 06/2015
- Google Mobile Sites Certification | 05/2017
